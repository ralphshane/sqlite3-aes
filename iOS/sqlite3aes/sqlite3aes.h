//
//  sqlite3aes.h
//  sqlite3aes
//
//  Created by Ralph Shane on 04/02/2017.
//  Copyright © 2017 Ralph Shane. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for sqlite3aes.
FOUNDATION_EXPORT double sqlite3aesVersionNumber;

//! Project version string for sqlite3aes.
FOUNDATION_EXPORT const unsigned char sqlite3aesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <sqlite3aes/PublicHeader.h>

#import <sqlite3aes/sqlite3.h>
