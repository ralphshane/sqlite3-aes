sqlite3-aes
===========

SQLite3 with AES 256.
----
    https://github.com/utelle/wxsqlite3


Checkout wxSQLite3
----
    git submodule update --init


Command
----
    .open sample.dbx
    pragma key='Abc123';
    CREATE TABLE peopleInfo(peopleInfoID integer primary key, firstname text, lastname text, age integer);    
    select * from peopleInfo;
